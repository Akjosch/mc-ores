package de.vernideas.mc.common;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.Supplier;

public final class FunctionUtil {
	private static final Supplier<MethodHandles.Lookup> universalLookup = memoize(() -> {
		try {
			Field f = MethodHandles.Lookup.class.getDeclaredField("IMPL_LOOKUP");
			f.setAccessible(true);
			return (MethodHandles.Lookup) f.get(null);
		} catch (Exception e) {
			return null;
		}
	});
	
	public static <T> Supplier<T> memoize(Supplier<T> original) {
		return new Supplier<T>() {
			Supplier<T> delegate = this::firstTime;
			boolean initialized;
			
			public T get() {
				return delegate.get();
			}
			
			private synchronized T firstTime() {
				if(!initialized) {
					T value = original.get();
					delegate=() -> value;
					initialized = true;
				}
				return delegate.get();
			}
		};
	}
	
	public static MethodHandle fieldGetterHandle(Class<?> cls, String field, Class<?> fieldCls) {
		MethodHandles.Lookup lookup = universalLookup.get().in(cls);
		try {
			return lookup.findGetter(cls, field, fieldCls);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static MethodHandle fieldSetterHandle(Class<?> cls, String field, Class<?> fieldCls) {
		MethodHandles.Lookup lookup = universalLookup.get().in(cls);
		try {
			return lookup.findSetter(cls, field, fieldCls);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static <T> Consumer<T> fieldSetter(Object obj, String field, Class<T> fieldCls) {
		final MethodHandle handle = fieldSetterHandle(obj.getClass(), field, fieldCls).bindTo(obj);
		return (val) -> { try { handle.invoke(val); } catch(Throwable e) { e.printStackTrace(); } };
	}

	public static IntConsumer fieldIntSetter(Object obj, String field) {
		final MethodHandle handle = fieldSetterHandle(obj.getClass(), field, int.class).bindTo(obj);
		return (val) -> { try { handle.invokeExact(val); } catch(Throwable e) { e.printStackTrace(); } };
	}
	
	public static BooleanConsumer fieldBooleanSetter(Object obj, String field) {
		final MethodHandle handle = fieldSetterHandle(obj.getClass(), field, boolean.class).bindTo(obj);
		return (val) -> { try { handle.invokeExact(val); } catch(Throwable e) { e.printStackTrace(); } };
	}

	@FunctionalInterface
	public static interface BooleanConsumer {
		void accept(boolean value);
	}
}
