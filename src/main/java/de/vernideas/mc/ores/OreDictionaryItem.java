package de.vernideas.mc.ores;

public interface OreDictionaryItem {
	String getOreName();
}
