package de.vernideas.mc.ores.item;

import de.vernideas.mc.ores.OreDictionaryItem;
import de.vernideas.mc.ores.Ores;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public abstract class OreDust extends Item implements OreDictionaryItem {
	protected String oreName;

	public static OreDust create(String id) {
		OreDust item = new OreDust() {
			@Override protected void initialize() {
				setTranslationKey(id + "_dust");
				setRegistryName(Ores.MODID, id + "_dust");
				oreName = "dust" + id.substring(0, 1).toUpperCase() + id.substring(1);
			}
		};
		return item;
	}
	
	public OreDust() {
		super();
		setCreativeTab(CreativeTabs.MATERIALS);
		initialize();
	}
	
	protected abstract void initialize();
	
	@Override public String getOreName() {
		return oreName;
	}
}
