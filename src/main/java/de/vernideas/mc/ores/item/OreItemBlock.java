package de.vernideas.mc.ores.item;

import de.vernideas.mc.ores.block.OreBlock;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class OreItemBlock extends ItemBlock {
	public OreItemBlock(Block block) {
		super(block);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
	}

	@Override public int getMetadata(int damage) {
		return damage;
	}
}
