package de.vernideas.mc.ores;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import de.vernideas.mc.common.FunctionUtil;
import de.vernideas.mc.ores.block.OreBlock;
import de.vernideas.mc.ores.item.OreDust;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;

@Mod.EventBusSubscriber(modid=Ores.MODID)
public class OreItems {
	public static final Supplier<Item> LIMONITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("limonite");
	});
	public static final Supplier<Item> HEMATITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("hematite");
	});
	public static final Supplier<Item> MAGNETITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("magnetite");
	});
	public static final Supplier<Item> ILMENITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("ilmenite");
	});
	public static final Supplier<Item> PENTLANDITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("pentlandite");
	});
	public static final Supplier<Item> GOETHITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("goethite");
	});
	public static final Supplier<Item> SIDERITE_DUST = FunctionUtil.memoize(() -> {
		return OreDust.create("siderite");
	});
	public static final List<Supplier<Item>> ALL_ITEMS = Arrays.asList(
		/* iron ore dusts */
		LIMONITE_DUST, HEMATITE_DUST, MAGNETITE_DUST, ILMENITE_DUST, PENTLANDITE_DUST, GOETHITE_DUST, SIDERITE_DUST
	);

	public static void init() {}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		ALL_ITEMS.stream()
			.map((sup) -> sup.get())
			.forEach((item) -> event.getRegistry().registerAll(item));
		ALL_ITEMS.stream()
			.map((sup) -> sup.get())
			.filter((item) -> item instanceof OreDictionaryItem && null != ((OreDictionaryItem)item).getOreName())
			.forEach((item) -> OreDictionary.registerOre(((OreDictionaryItem)item).getOreName(), item));
	}
	
	@SubscribeEvent
	public static void registerRenders(ModelRegistryEvent event) {
		ALL_ITEMS.stream()
			.map((sup) -> sup.get())
			.forEach((item) -> ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory")));
	}
}
