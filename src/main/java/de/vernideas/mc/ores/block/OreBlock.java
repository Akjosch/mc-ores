package de.vernideas.mc.ores.block;

import java.util.Collection;
import java.util.Random;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Optional;

import de.vernideas.mc.common.FunctionUtil;
import de.vernideas.mc.ores.OreDictionaryItem;
import de.vernideas.mc.ores.Ores;
import de.vernideas.mc.ores.PostRegInitializer;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyHelper;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public abstract class OreBlock extends Block implements OreDictionaryItem, PostRegInitializer {
	protected String dustName = null;
	protected String oreName = null;
	protected Predicate<OreBlockBase> baseFilter = null;
	protected IProperty<OreBlockBase> bases = null;
	private Supplier<ItemStack> dust = FunctionUtil.memoize(() -> OreDictionary.getOres(dustName, false).get(0));
	
	public static OreBlock create(Material mat, String id) {
		return create(mat, id, null);
	}
	
	public static OreBlock create(Material mat, String id, OreBlockBase[] newBases) {
		OreBlock block = new OreBlock(mat) {
			@Override public void initialize() {
				setTranslationKey(id + "_ore");
				setHardness(1f);
				setHarvestLevel("pickaxe", 1);
				dustName = "dust" + id.substring(0, 1).toUpperCase() + id.substring(1);
				oreName = "ore" + id.substring(0, 1).toUpperCase() + id.substring(1);
			}
			
			@Override protected BlockStateContainer createBlockState() {
				if(null != newBases) {
					baseFilter = (b -> ArrayUtils.indexOf(newBases, b) > -1);
					bases = new PropertyHelper<OreBlockBase>("base", OreBlockBase.class) {
						@Override public Collection<OreBlockBase> getAllowedValues() {
							return Stream.of(OreBlockBase.values()).filter(baseFilter).collect(Collectors.toList());
						}

						@Override public Optional<OreBlockBase> parseValue(String value) {
							return Optional.fromNullable(OreBlockBase.byName(value));
						}

						@Override public String getName(OreBlockBase value) {
							return value.name;
						}
					};
					return new BlockStateContainer(this, bases);
				} else {
					return super.createBlockState();
				}
			}
		};
		block.setRegistryName(Ores.MODID, id + "_ore");
		if(null != newBases) {
			block.baseFilter = (b -> ArrayUtils.indexOf(newBases, b) > -1);
			block.bases = new PropertyHelper<OreBlockBase>("base", OreBlockBase.class) {
				@Override public Collection<OreBlockBase> getAllowedValues() {
					return Stream.of(OreBlockBase.values()).filter(block.baseFilter).collect(Collectors.toList());
				}

				@Override public Optional<OreBlockBase> parseValue(String value) {
					return Optional.fromNullable(OreBlockBase.byName(value));
				}

				@Override public String getName(OreBlockBase value) {
					return value.name;
				}
			};
		}
		return block;
	}
	
	public OreBlock() {
		this((Material)null);
	}
	
	public OreBlock(Material mat) {
		super(null == mat ? Material.ROCK : mat);
		setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		setSoundType(SoundType.STONE);
	}
	
	@Override public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
		ItemStack dustDrop = dust.get();
		if(null != dustDrop) {
			dustDrop = dustDrop.copy();
			Random rand = world instanceof World ? ((World)world).rand : RANDOM;
			int amount = Math.floorDiv(rand.nextInt(fortune * 2 + 5), 3) + 1;
			if(amount > 8) {
				amount = 8;
			}
			dustDrop.setCount(amount);
			drops.add(dustDrop);
		}
	}
	
	@Override public int damageDropped(IBlockState state) {
		return getMetaFromState(state);
	}
	
	@Override public String getOreName() {
		return oreName;
	}
	
	@Override public IBlockState getStateFromMeta(int meta) {
		if(null != bases) {
			return getDefaultState().withProperty(bases, OreBlockBase.byMeta(meta));
		} else {
			return super.getStateFromMeta(meta);
		}
	}

	@Override public int getMetaFromState(IBlockState state) {
		if(null != bases) {
			return state.getValue(bases).meta;
		} else {
			return super.getMetaFromState(state);
		}
	}
	
	@Override public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
		if(null != bases) {
			for (final OreBlockBase base : OreBlockBase.values()) {
				if(baseFilter.test(base)) {
					items.add(new ItemStack(this, 1, base.meta));
				}
			}
		} else {
			super.getSubBlocks(itemIn, items);
		}
	}
}
