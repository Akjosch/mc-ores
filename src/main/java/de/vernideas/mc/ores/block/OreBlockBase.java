package de.vernideas.mc.ores.block;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OreBlockBase {
	STONE(0, "stone"),
	DIRT(1, "dirt"),
	GRAVEL(2, "gravel"),
	SAND(3, "sand"),
	GRANITE(4, "granite"),
	ANDESITE(5, "andesite"),
	DIORITE(6, "diorite"),
	BASALT(7, "basalt"),
	MARBLE(8, "marble"),
	LIMESTONE(9, "limestone"),
	SLATE(10, "slate"),
	SANDSTONE(11, "sandstone"),
	JASPER(12, "jasper");
	
	private static final OreBlockBase[] META_LOOKUP
		= Stream.of(values())
			.sorted(Comparator.comparing(b -> b.meta))
			.toArray(OreBlockBase[]::new);
	private static final Map<String, OreBlockBase> NAME_MAP
		= Stream.of(values())
			.collect(Collectors.toMap(b -> b.name, Function.identity()));
	
	public static OreBlockBase byMeta(int meta) {
		if(meta < 0 || meta >= META_LOOKUP.length) {
			meta = 0;
		}
		return META_LOOKUP[meta];
	}
	
	public static OreBlockBase byName(String name) {
		return NAME_MAP.get(name);
	}
	
	public final int meta;
	public final String name;
	
	private OreBlockBase(int meta, String name) {
		this.meta = meta;
		this.name = name;
	}
}