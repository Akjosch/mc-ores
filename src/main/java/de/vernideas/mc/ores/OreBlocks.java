package de.vernideas.mc.ores;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import de.vernideas.mc.common.FunctionUtil;
import de.vernideas.mc.ores.block.OreBlockBase;
import de.vernideas.mc.ores.item.OreItemBlock;
import de.vernideas.mc.ores.block.OreBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;

@Mod.EventBusSubscriber(modid=Ores.MODID)
public class OreBlocks {
	public static final Supplier<Block> LIMONITE_ORE = FunctionUtil.memoize(() -> {
		Block block = OreBlock.create(Material.GROUND, "limonite");
		block.setHarvestLevel("shovel", 0);
		return block;
	});
	public static final Supplier<Block> HEMATITE_ORE = FunctionUtil.memoize(() -> {
		return OreBlock.create(Material.ROCK, "hematite",
			new OreBlockBase[] {OreBlockBase.STONE, OreBlockBase.SANDSTONE, OreBlockBase.LIMESTONE});
	});
	public static final Supplier<Block> MAGNETITE_ORE = FunctionUtil.memoize(() -> {
		return OreBlock.create(Material.ROCK, "magnetite");
	});
	public static final Supplier<Block> ILMENITE_ORE = FunctionUtil.memoize(() -> {
		return OreBlock.create(Material.ROCK, "ilmenite");
	});
	public static final Supplier<Block> PENTLANDITE_ORE = FunctionUtil.memoize(() -> {
		return OreBlock.create(Material.ROCK, "pentlandite");
	});
	public static final Supplier<Block> GOETHITE_ORE = FunctionUtil.memoize(() -> {
		Block block = OreBlock.create(Material.SAND, "goethite");
		block.setHarvestLevel("shovel", 1);
		return block;
	});
	public static final Supplier<Block> SIDERITE_ORE = FunctionUtil.memoize(() -> {
		return OreBlock.create(Material.ROCK, "siderite");
	});
	public static final List<Supplier<Block>> ALL_BLOCKS = Arrays.asList(
		/* iron ores */
		LIMONITE_ORE, HEMATITE_ORE, MAGNETITE_ORE, ILMENITE_ORE, PENTLANDITE_ORE, GOETHITE_ORE, SIDERITE_ORE
	);
	
	public static void init() {}
	
	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		ALL_BLOCKS.stream()
			.map((sup) -> sup.get())
			.forEach((block) -> {
				event.getRegistry().registerAll(block);
				if(block instanceof PostRegInitializer) {
					((PostRegInitializer) block).initialize();
				}
			});
	}
	
	@SubscribeEvent
	public static void registerItemBlocks(RegistryEvent.Register<Item> event) {
		ALL_BLOCKS.stream()
			.map((sup) -> sup.get())
			.forEach((block) -> event.getRegistry().registerAll(new OreItemBlock(block).setRegistryName(block.getRegistryName())));
		ALL_BLOCKS.stream()
			.map((sup) -> sup.get())
			.filter((block) -> block instanceof OreDictionaryItem && null != ((OreDictionaryItem)block).getOreName())
			.forEach((block) -> OreDictionary.registerOre(((OreDictionaryItem)block).getOreName(), block));
	}
	
	@SubscribeEvent
	public static void registerRenders(ModelRegistryEvent event) {
		ALL_BLOCKS.stream()
			.map((sup) -> Item.getItemFromBlock(sup.get()))
			.forEach((item) -> ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory")));
	}
}
