package de.vernideas.mc.ores;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Ores.MODID, version = Ores.VERSION)
public class Ores {
    public static final String MODID = "ores";
    public static final String VERSION = "1.0";

    @EventHandler
    public void init(FMLPreInitializationEvent event) {
    	OreBlocks.init();
    	OreItems.init();
    }
}
